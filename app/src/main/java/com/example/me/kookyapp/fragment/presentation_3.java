package com.example.me.kookyapp.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.me.kookyapp.R;

import java.util.zip.Inflater;

/**
 * Created by Ä.M.E on 15/03/2017.
 */
public class presentation_3 extends Fragment {
    View vue_pres3;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        vue_pres3 = inflater.inflate(R.layout.presentation_3_layout, container, false);
        return vue_pres3;
    }
}
