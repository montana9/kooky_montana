package com.example.me.kookyapp.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.me.kookyapp.R;

/**
 * Created by Ä.M.E on 15/03/2017.
 */
public class Presentation extends Fragment{
    View vue_prest;
    @Nullable

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        vue_prest=inflater.inflate(R.layout.presentation_layout,container,false);
        return vue_prest;

}
}