package com.example.me.kookyapp.adapter;

import android.content.Context;
import android.support.v4.app.*;
import android.support.v4.app.Fragment;

import com.example.me.kookyapp.fragment.Presentation;
import com.example.me.kookyapp.fragment.Presentation_2;
import com.example.me.kookyapp.fragment.presentation_3;
import com.example.me.kookyapp.fragment.presentation_4;

/**
 * Created by Ä.M.E on 15/03/2017.
 */

public class Swipe  extends FragmentPagerAdapter {
    public String fragments[] = {"fragment 1", "fragment 2", "fragment 3 ", "fragment 4"};

    public Swipe(FragmentManager fm, Context ctx) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new Presentation();
            case 1:
                return new Presentation_2();
            case 2:
                return new presentation_3();
            case 3:
                return new presentation_4();
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return fragments.length;
    }
}
