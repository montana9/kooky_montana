package com.example.me.kookyapp;

import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.ScrollView;

import android.widget.TextView;

import com.example.me.kookyapp.adapter.Swipe;


public class MainActivity extends AppCompatActivity {
    private ScrollView scroll;
    private TextView nom;
    private TextView prenom;
    private TextView age;
    private TextView fonction;
    private ImageView imageView;
    TabLayout tableLayout;
    ViewPager viewPager;
    Swipe swipe;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        correspondance();
        viewPager = (ViewPager) findViewById(R.id.id_view_pager);
        tableLayout = (TabLayout) findViewById(R.id.tabs);
        tableLayout.setupWithViewPager(viewPager);
        tableLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());

            }
        });
        appelImage();

    }

    private void correspondance(){



    }

    public void appelImage(){
        ViewPager viewPager = (ViewPager) findViewById(R.id.id_view_pager);
        viewPager.setAdapter(new Swipe(getSupportFragmentManager(), getApplicationContext()));
    }








    }

