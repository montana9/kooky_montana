package com.example.me.kookyapp.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.me.kookyapp.R;

/**
 * Created by Ä.M.E on 15/03/2017.
 */

public class Presentation_2 extends Fragment {
    View vue_prest2;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        vue_prest2=inflater.inflate(R.layout.presentation_2_layout,container,false);
        return vue_prest2;


    }
}
